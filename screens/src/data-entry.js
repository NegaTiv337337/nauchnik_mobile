import React, { useState, useEffect } from "react";
import { Form, Button, Modal, Alert } from "react-bootstrap";
import { Form as FForm, Field } from "react-final-form";
import arrayMutators from "final-form-arrays";
import { FieldArray } from "react-final-form-arrays";
import Creatable from "react-select/creatable";
import { connect, useDispatch } from "react-redux";
import { handleFormSubmit } from "./redux/actions";
import Loader from "react-loader-spinner";
import { CustomSelector } from "./components/CustomSelector";
import "./data-entry.css";

const ARTICLE_TYPES = [
  { label: "Scopus (WoS)", value: "Scopus (WoS)" },
  { label: "ВАК", value: "ВАК" },
  { label: "РИНЦ", value: "РИНЦ" },
  { label: "Без БД", value: "Без БД" },
];

const GUIDE_TYPES = [
  { label: "Учебное", value: "Учебное" },
  { label: "Учебно-методическое", value: "Учебно-методическое" },
  { label: "Методическое", value: "Методическое" },
];

const CONFERENCE_TYPES = [
  { label: "Международная", value: "Международная" },
  { label: "Всероссийская", value: "Всероссийская" },
  { label: "Региональная", value: "Региональная" },
];

let YEARS = [{ value: null, label: "Нет информации" }];

for (let i = 2000; i < 2030; i++) {
  YEARS.push({ value: i, label: i });
}

const CoAuthors = ({ input, ...rest }) => {
  const formatCreateLabel = (str) => `Добавить "${str}"`;
  const noOptionsMessage = () => "Введите ФИО соавтора";
  const [hasCoAuthors, setHasCoAuthors] = useState(false);
  const onCheckboxChange = (e) => {
    setHasCoAuthors(e.target.checked);
  };
  return (
    <div>
      <div className="d-flex align-items-center">
        <Form.Check onChange={onCheckboxChange} type="checkbox" />
        <Form.Label>Есть соавторы</Form.Label>
      </div>
      {hasCoAuthors && (
        <Creatable
          noOptionsMessage={noOptionsMessage}
          className="selector"
          formatCreateLabel={formatCreateLabel}
          isMulti
          placeholder="Денисов Д.Э."
          classNamePrefix="select"
          {...input}
          {...rest}
        />
      )}
    </div>
  );
};

export const DataEntry = connect((state) => ({
  isSubmitting: state.isSubmitting,
  dataFormError: state.dataFormError,
  isFormSuccessful: state.isFormSuccessful,
}))(({ isSubmitting, dataFormError, isFormSuccessful }) => {
  const dispatch = useDispatch();
  const [frontError, setFrontError] = useState("");
  const onSubmit = (data) => {
    console.log(data);
    let errors = validate(data);
    console.log(errors);
    if (errors.length > 0) {
      setFrontError(errors.join("\n"));
    } else {
      dispatch(handleFormSubmit(data));
    }
  };
  const validate = (values) => {
    let errors = [];
    if (values.articles && values.articles.length > 0) {
      values.articles.forEach((el, idx) => {
        if (el && (!el.info || el.info.length === 0)) {
          errors.push(`Статья №${idx + 1}: поле "информация" нужно заполнить.`);
        }
      });
    }
    if (values.guides && values.guides.length > 0) {
      values.guides.forEach((el, idx) => {
        if (el && (!el.info || el.info.length === 0)) {
          errors.push(
            `Пособие №${idx + 1}: поле "информация" нужно заполнить.`
          );
        }
      });
    }
    if (values.conferences && values.conferences.length > 0) {
      values.conferences.forEach((el, idx) => {
        if (
          el &&
          (!el.theme || (el.theme.value && el.theme.value.length === 0))
        ) {
          errors.push(
            `Конференция №${idx + 1}: поле "тема доклада" нужно заполнить.`
          );
        }
      });
    }
    return errors;
  };
  const retry = () => {
    window.location.reload();
  };

  return (
    <FForm
      onSubmit={onSubmit}
      mutators={{
        ...arrayMutators,
      }}
      render={({
        handleSubmit,
        form: {
          mutators: { push, remove },
        },
        pristine,
        form,
        submitting,
        values,
      }) => {
        const isNotEmpty = Object.values(values).some((a) => a.length > 0);
        console.log(values, isNotEmpty);
        return (
          <>
            {!isSubmitting && !dataFormError && !isFormSuccessful && (
              <form onSubmit={handleSubmit}>
                <div className="main-states">
                  <div className="head-states">
                    <h2 className="heading">Статьи в журналах</h2>
                    <Button
                      onClick={() => {
                        push("articles", undefined);
                      }}
                      variant="success"
                      type="button"
                      className="add-states"
                    >
                      Добавить
                    </Button>
                  </div>
                  <FieldArray name="articles">
                    {({ fields }) =>
                      fields.map((el, idx) => {
                        console.log(el);
                        return (
                          <div key={el} className="d-flex flex-column">
                            <div className="states-container justify-content-center">
                              <h4>№{idx + 1}</h4>
                              <div className="pre-states-container">
                                <div className="plus-info input__group">
                                  <Form.Group>
                                    <Form.Label>
                                      Информация{" "}
                                      <span className="asterisk">*</span>
                                    </Form.Label>
                                    <Field
                                      name={`${el}.info`}
                                      className="form-control"
                                      rows="3"
                                      component="textarea"
                                      type="text"
                                    />
                                  </Form.Group>
                                </div>
                                <div className="place-from input__group">
                                  <Field
                                    name={`${el}.company`}
                                    options={ARTICLE_TYPES}
                                    label="Выберите площадку"
                                    defaultValue={ARTICLE_TYPES[0]}
                                    placeholder="ВАК"
                                    required
                                    isInvalid={true}
                                    component={CustomSelector}
                                  />
                                </div>
                                <div className="publication-link-form input__group">
                                  <Form.Group>
                                    <Form.Label>
                                      Ссылка на публикацию
                                    </Form.Label>
                                    <Field
                                      name={`${el}.link`}
                                      component="input"
                                      className="form-control"
                                      placeholder="https://drive.google.com/?p=1"
                                    />
                                  </Form.Group>
                                </div>
                                <div className="input__group">
                                  <Field
                                    name={`${el}.year`}
                                    options={YEARS}
                                    placeholder="2020"
                                    label="Год публикации"
                                    component={CustomSelector}
                                  />
                                </div>
                                <div className="input__group">
                                  <Field
                                    name={`${el}.coAuthors`}
                                    component={CoAuthors}
                                  />
                                </div>
                              </div>
                            </div>
                            <Button
                              onClick={() => {
                                remove("articles", idx);
                              }}
                              variant="danger"
                              type="button"
                              className="delete-states"
                            >
                              Удалить
                            </Button>
                          </div>
                        );
                      })
                    }
                  </FieldArray>
                </div>
                <div className="main-states">
                  <div className="head-states">
                    <h2 className="heading">Пособия</h2>
                    <Button
                      onClick={() => {
                        push("guides", undefined);
                      }}
                      variant="success"
                      type="button"
                      className="add-states"
                    >
                      Добавить
                    </Button>
                  </div>
                  <FieldArray name="guides">
                    {({ fields }) =>
                      fields.map((el, idx) => {
                        console.log(el);
                        return (
                          <div key={el} className="d-flex flex-column">
                            <div className="states-container justify-content-center">
                              <h4>№{idx + 1}</h4>
                              <div className="pre-states-container">
                                <div className="plus-info input__group">
                                  <Form.Group>
                                    <Form.Label>
                                      Информация{" "}
                                      <span className="asterisk">*</span>
                                    </Form.Label>
                                    <Field
                                      name={`${el}.info`}
                                      className="form-control"
                                      rows="3"
                                      component="textarea"
                                      type="text"
                                    />
                                  </Form.Group>
                                </div>
                                <div className="place-from input__group">
                                  <Field
                                    name={`${el}.type`}
                                    options={GUIDE_TYPES}
                                    label="Выберите тип пособия"
                                    component={CustomSelector}
                                    defaultValue={GUIDE_TYPES[0]}
                                    required
                                    placeholder="Учебное"
                                  />
                                </div>
                                <div className="publication-link-form input__group">
                                  <Form.Group>
                                    <Form.Label>ISBN</Form.Label>
                                    <Field
                                      name={`${el}.isbn`}
                                      component="input"
                                      className="form-control"
                                      placeholder="978-3-16-148410-0"
                                    />
                                  </Form.Group>
                                </div>
                                <div className="input__group">
                                  <Field
                                    name={`${el}.year`}
                                    options={YEARS}
                                    placeholder="2020"
                                    label="Год публикации"
                                    component={CustomSelector}
                                  />
                                </div>
                                <div className="input__group">
                                  <Field
                                    name={`${el}.coAuthors`}
                                    component={CoAuthors}
                                  />
                                </div>
                              </div>
                            </div>
                            <Button
                              onClick={() => {
                                remove("guides", idx);
                              }}
                              variant="danger"
                              type="button"
                              className="delete-states"
                            >
                              Удалить
                            </Button>
                          </div>
                        );
                      })
                    }
                  </FieldArray>
                </div>
                <div className="main-states">
                  <div className="head-states">
                    <h2 className="heading">Конференции</h2>
                    <Button
                      onClick={() => {
                        push("conferences", undefined);
                      }}
                      variant="success"
                      type="button"
                      className="add-states"
                    >
                      Добавить
                    </Button>
                  </div>
                  <FieldArray name="conferences">
                    {({ fields }) =>
                      fields.map((el, idx) => {
                        console.log(el);
                        return (
                          <div key={el} className="d-flex flex-column">
                            <div className="states-container justify-content-center">
                              <h4>№{idx + 1}</h4>
                              <div className="pre-states-container">
                                <div className="plus-info input__group">
                                  <Form.Group>
                                    <Form.Label>Информация</Form.Label>
                                    <Field
                                      name={`${el}.info`}
                                      className="form-control"
                                      rows="3"
                                      component="textarea"
                                      type="text"
                                    />
                                  </Form.Group>
                                </div>
                                <div className="place-from input__group">
                                  <Field
                                    name={`${el}.type`}
                                    options={CONFERENCE_TYPES}
                                    label="Выберите тип конференции"
                                    component={CustomSelector}
                                    defaultValue={CONFERENCE_TYPES[0]}
                                    placeholder="Учебное"
                                    required
                                  />
                                </div>
                                <div className="publication-link-form input__group">
                                  <Form.Group>
                                    <Form.Label>
                                      Название доклада{" "}
                                      <span className="asterisk">*</span>
                                    </Form.Label>
                                    <Field name={`${el}.theme`}>
                                      {({ input, meta }) => (
                                        <>
                                          <input
                                            {...input}
                                            className="form-control"
                                            placeholder="Нахождение расстояния между прямыми"
                                          />
                                          {meta.error && meta.touched && (
                                            <span>{meta.error}</span>
                                          )}
                                        </>
                                      )}
                                    </Field>
                                  </Form.Group>
                                </div>
                                <div className="input__group">
                                  <Field
                                    name={`${el}.year`}
                                    options={YEARS}
                                    placeholder="2020"
                                    label="Год публикации"
                                    component={CustomSelector}
                                  />
                                </div>
                                <div className="input__group">
                                  <Field
                                    name={`${el}.coAuthors`}
                                    component={CoAuthors}
                                  />
                                </div>
                              </div>
                            </div>
                            <Button
                              onClick={() => {
                                remove("conferences", idx);
                              }}
                              variant="danger"
                              type="button"
                              className="delete-states"
                            >
                              Удалить
                            </Button>
                          </div>
                        );
                      })
                    }
                  </FieldArray>
                </div>
                <div className="justify-content-center send-form">
                  <Button
                    size="lg"
                    variant="primary"
                    className="send"
                    type="submit"
                    disabled={submitting || pristine || !isNotEmpty}
                  >
                    Отправить
                  </Button>
                </div>
              </form>
            )}
            {isSubmitting && (
              <div className="allscreen">
                <Loader
                  type="Oval"
                  color="#00BFFF"
                  height={100}
                  width={100}
                  timeout={0}
                />
              </div>
            )}
            {isFormSuccessful && (
              <div className="alert-root">
                <Alert variant="success">
                  Форма была успешно отправлена. Спасибо!
                </Alert>
                <Button onClick={retry} variant="primary">
                  Отправить ещё
                </Button>
              </div>
            )}
            <Modal
              show={frontError.length > 0}
              onHide={() => {
                setFrontError("");
              }}
            >
              <Modal.Header closeButton>
                <Modal.Title>Ошибка</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Alert variant="danger">{frontError}</Alert>
              </Modal.Body>
              <Modal.Footer>
                <Button
                  variant="primary"
                  onClick={() => {
                    setFrontError("");
                  }}
                >
                  Ок
                </Button>
              </Modal.Footer>
            </Modal>
          </>
        );
      }}
    />
  );
});
