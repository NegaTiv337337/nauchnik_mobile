import { http } from "../api/http.js";
import qs from "qs";
import { toast } from "react-toastify";

toast.configure({
  autoClose: 3000
});

export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";

export const REMIND_FROM_LS = "REMIND_FROM_LS";

export const FORM_DATA_REQUEST = "FORM_DATA_REQUEST";
export const FORM_DATA_SUCCESS = "FORM_DATA_SUCCESS";
export const FORM_DATA_FAIL = "FORM_DATA_FAIL";

export const REPORT_REQUEST = "REPORT_REQUEST";
export const REPORT_SUCCESS = "REPORT_SUCCESS";
export const REPORT_FAIL = "REPORT_FAIL";

export const USER_LOGOUT = "USER_LOGOUT";
export const CHANGE_FORM_USER = "CHANGE_FORM_USER";

export const USER_GET_REQUEST = "USER_GET_REQUEST";
export const USER_GET_SUCCESS = "USER_GET_SUCCESS";
export const USER_GET_FAIL = "USER_GET_FAIL";

export const USERS_GET_REQUEST = "USERS_GET_REQUEST";
export const USERS_GET_SUCCESS = "USERS_GET_SUCCESS";
export const USERS_GET_FAIL = "USERS_GET_FAIL";

export const FORM_REQUEST = "FORM_REQUEST";
export const FORM_SUCCESS = "FORM_SUCCESS";
export const FORM_FAIL = "FORM_FAIL";

export const USER_DELETE_REQUEST = "USER_DELETE_REQUEST";
export const USER_DELETE_SUCCESS = "USER_DELETE_SUCCESS";
export const USER_DELETE_FAIL = "USER_DELETE_FAIL";

export const USER_EDIT_REQUEST = "USER_EDIT_REQUEST";
export const USER_EDIT_SUCCESS = "USER_EDIT_SUCCESS";
export const USER_EDIT_FAIL = "USER_EDIT_FAIL";

export const USER_ADD_REQUEST = "USER_ADD_REQUEST";
export const USER_ADD_SUCCESS = "USER_ADD_SUCCESS";
export const USER_ADD_FAIL = "USER_ADD_FAIL";

export const CHANGE_FILTERS = "CHANGE_FILTERS";

export function handleFormSubmit(form) {
  return function(dispatch) {
    dispatch({
      type: FORM_REQUEST
    });
    console.log(form);
    let obj = {};
    if (form.articles && form.articles.length > 0) {
      obj.articles = form.articles.map(el => {
        let coAuthors = [];
        if (el.coAuthors && el.coAuthors.length > 0) {
          coAuthors = [{ name: el.coAuthors.map(a => a.value).join(", ") }];
        }
        let newObj = {
          ...el,
          info: el.info || " ",
          company: el.company.value,
          coAuthors
        };
        newObj.year = el.year && el.year.value ? String(el.year.value) : " ";
        return newObj;
      });
    }
    if (form.guides && form.guides.length > 0) {
      obj.guides = form.guides.map(el => {
        let coAuthors = [];
        if (el.coAuthors && el.coAuthors.length > 0) {
          coAuthors = [{ name: el.coAuthors.map(a => a.value).join(", ") }];
        }
        let newObj = {
          ...el,
          info: el.info || " ",
          type: el.type.value,
          coAuthors
        };
        newObj.year = el.year && el.year.value ? String(el.year.value) : " ";
        return newObj;
      });
    }
    if (form.conferences && form.conferences.length > 0) {
      obj.conferences = form.conferences.map(el => {
        let coAuthors = [];
        if (el.coAuthors && el.coAuthors.length > 0) {
          coAuthors = [{ name: el.coAuthors.map(a => a.value).join(", ") }];
        }
        let newObj = {
          ...el,
          info: el.info || " ",
          type: el.type.value,
          coAuthors
        };
        newObj.year = el.year && el.year.value ? String(el.year.value) : " ";
        return newObj;
      });
    }
    console.log(form, obj);
    http
      .post("/user/data", obj)
      .then(d => {
        const { data } = d;
        console.log(d);
        dispatch({ type: FORM_SUCCESS });
      })
      .catch(err => {
        console.log(err);
        if (err.response && err.response.data) {
          dispatch({
            type: FORM_FAIL,
            payload: {
              message: `${err.response.data.error} ${err.response.data.details}`
            }
          });
        } else {
          dispatch({
            type: FORM_FAIL,
            payload: { message: "Форма не отправилась. Ошибка." }
          });
        }
      });
  };
}

export function changeFormUser(id) {
  return {
    type: CHANGE_FORM_USER,
    payload: id
  };
}

export function handleLogout() {
  localStorage.removeItem("NAUCHNIK-JWT");
  localStorage.removeItem("NAUCHNIK-USER");
  window.location.reload();
  return { type: USER_LOGOUT };
}

export function remindFromLS(token, user) {
  http.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  return {
    type: REMIND_FROM_LS,
    payload: { user }
  };
}

export function deleteUser(id) {
  return function(dispatch) {
    dispatch({
      type: USER_DELETE_REQUEST
    });

    http
      .delete("/admin/" + id)
      .then(d => {
        const { data } = d;
        console.log(d);
        if (data) {
          toast.success("Вы успешно удалили пользователя.", {
            position: toast.POSITION.BOTTOM_CENTER
          });
          dispatch({
            type: USER_DELETE_SUCCESS
          });
          dispatch(getUsersList());
        }
      })
      .catch(err => {
        console.error(err);
        dispatch({
          type: USER_DELETE_FAIL,
          payload: { message: "Ошибка удаления пользователя." }
        });
      });
  };
}

export function addUser(form) {
  return function(dispatch) {
    dispatch({
      type: USER_ADD_REQUEST
    });
    let values = form;
    values.role = form.role.value;
    http
      .post("/admin/", values)
      .then(d => {
        const { data } = d;
        console.log(d);
        if (data) {
          toast.success("Вы успешно добавили пользователя.", {
            position: toast.POSITION.BOTTOM_CENTER
          });
          dispatch({
            type: USER_ADD_SUCCESS
          });
          dispatch(changeFormUser(""));
          dispatch(getUsersList());
        }
      })
      .catch(err => {
        console.error(err);
        if (err.response && err.response.data) {
          dispatch({
            type: USER_ADD_FAIL,
            payload: {
              message: `Ошибка добавления пользователя: ${err.response.data.error} ${err.response.data.details}`
            }
          });
        } else {
          dispatch({
            type: USER_ADD_FAIL,
            payload: { message: "Неизвестная ошибка добавления пользователя." }
          });
        }
      });
  };
}

export function changeFilters(params) {
  return dispatch => {
    dispatch({
      type: CHANGE_FILTERS,
      payload: params
    });
    dispatch(getUsersList());
  };
}

export function editUser(id, data) {
  return function(dispatch) {
    dispatch({
      type: USER_EDIT_REQUEST
    });
    let values = data;
    values.role = data.role.value;
    http
      .put("/admin/" + id, values)
      .then(d => {
        const { data } = d;
        console.log(d);
        if (data) {
          toast.success("Вы успешно отредактировали пользователя.", {
            position: toast.POSITION.BOTTOM_CENTER
          });
          dispatch({
            type: USER_EDIT_SUCCESS
          });
          dispatch(changeFormUser(""));
          dispatch(getUsersList());
        }
      })
      .catch(err => {
        console.error(err);
        if (err.response && err.response.data) {
          dispatch({
            type: USER_EDIT_FAIL,
            payload: {
              message: `Ошибка изменения пользователя: ${err.response.data.error} ${err.response.data.details}`
            }
          });
        } else {
          dispatch({
            type: USER_EDIT_FAIL,
            payload: { message: "Неизвестная ошибка изменения пользователя." }
          });
        }
      });
  };
}

export function getUsersList() {
  return function(dispatch, getState) {
    dispatch({
      type: USERS_GET_REQUEST
    });

    http
      .get("/admin/" + qs.stringify(), { params: getState().filters })
      .then(d => {
        const { data } = d;
        console.log(d);
        if (data) {
          dispatch({
            type: USERS_GET_SUCCESS,
            payload: data
          });
        }
      })
      .catch(err => {
        console.error(err);
        dispatch({
          type: USERS_GET_FAIL,
          payload: { message: "Ошибка получения списка пользователей." }
        });
      });
  };
}

export function handleLogin(cred) {
  return function(dispatch) {
    dispatch({
      type: LOGIN_REQUEST
    });

    http
      .post("/login", cred)
      .then(d => {
        const { data } = d;
        console.log(d);
        if (data && data.token && data.user) {
          const { token, user } = data;
          localStorage.setItem("NAUCHNIK-JWT", token);
          localStorage.setItem("NAUCHNIK-USER", JSON.stringify(user));
          http.defaults.headers.common["Authorization"] = `Bearer ${token}`;
          dispatch({ type: LOGIN_SUCCESS, payload: { user } });
          // window.location.pathname = '/'
        } else {
          dispatch({
            type: LOGIN_FAIL,
            payload: { message: "Неправильный логин или пароль." }
          });
        }
      })
      .catch(err => {
        console.error(err);
        dispatch({
          type: LOGIN_FAIL,
          payload: { message: "Неправильный логин или пароль." }
        });
      });
  };
}
