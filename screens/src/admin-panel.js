import React, { useState, useEffect } from "react";
import {
  FormControl,
  Button,
  Table,
  Row,
  Col,
  Container,
  Form,
  Alert,
} from "react-bootstrap";
import { Form as FForm, Field } from "react-final-form";
import { useSelector, useDispatch } from "react-redux";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./admin-panel.css";
import {
  getUsersList,
  addUser,
  editUser,
  deleteUser,
  changeFilters,
  changeFormUser,
} from "./redux/actions.js";
import useThrottledEffect from "use-throttled-effect";
import Loader from "react-loader-spinner";
import { CustomSelector } from "./components/CustomSelector";

const handleRole = (role) => {
  switch (role) {
    case "admin":
      return "Администратор";
    case "manager":
      return "Учитель";
    default:
      return role;
  }
};

const ROLES_OPTIONS = [
  { label: "Учитель", value: "manager" },
  { label: "Администратор", value: "admin" },
];

const required = (value) => (value ? undefined : "Обязательное поле.");
const mustBeNumber = (value) => (isNaN(value) ? "Must be a number" : undefined);
const minValue = (min) => (value) =>
  isNaN(value) || value >= min ? undefined : `Should be greater than ${min}`;
const minLength = (min) => (value) =>
  value.length >= min ? undefined : `Длина должна быть больше ${min}`;
const mustBeEmail = (value) =>
  /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/.test(value)
    ? undefined
    : "Должна быть правильно написанная электронная почта.";
const composeValidators = (...validators) => (value) =>
  validators.reduce((error, validator) => error || validator(value), undefined);

export const AdminPanel = (props) => {
  const dispatch = useDispatch();
  const [search, setSearch] = useState("");
  const getUsersListError = useSelector((state) => state.getUsersListError);
  const usersList = useSelector((state) => state.usersList);
  const selectedUserForm = useSelector((state) => state.selectedUserForm);
  const userAdminError = useSelector((state) => state.userAdminError);
  const isFetchingUsersList = useSelector((state) => state.isFetchingUsersList);
  const isRequestWorking = useSelector((state) => state.isRequestWorking);
  const setSelectedUserForm = (id) => {
    dispatch(changeFormUser(id));
  };
  const getUsers = () => {
    let params = {};
    if (search !== "") params.fio = search;
    dispatch(getUsersList(params));
  };
  const onAdd = (data) => {
    dispatch(addUser(data));
  };
  const onEdit = (id, data) => {
    dispatch(editUser(id, data));
  };
  const onDelete = (id) => {
    dispatch(deleteUser(id));
  };
  const handleCloseModal = () => {
    setSelectedUserForm("");
  };
  const onSubmit = (values) => {
    console.log(values);
    selectedUserForm == "create"
      ? onAdd(values)
      : onEdit(selectedUserForm, values);
  };
  const foundUser = usersList.find((a) => a._id == selectedUserForm);
  useEffect(() => {
    dispatch(changeFilters({ fio: search }));
  }, [search]);
  return (
    <div className="main-edit">
      <div className="search-bar">
        <Container>
          <Row>
            <Col sm={10}>
              <div className="s-i">
                <FormControl
                  onChange={(e) => setSearch(e.target.value)}
                  value={search}
                  type="text"
                  placeholder="Поиск по ФИО"
                  className="mr-sm-2 s-i"
                />
                <Button onClick={getUsers}>Искать</Button>
              </div>
            </Col>
            <Col sm={2}>
              <Button
                variant="success"
                onClick={() => {
                  setSelectedUserForm("create");
                }}
              >
                Добавить
              </Button>
            </Col>
          </Row>
        </Container>
      </div>
      <Container fluid>
        {!isFetchingUsersList && (
          <Table striped hover>
            <thead>
              <tr>
                <th>ФИО</th>
                <th>Должность</th>
                <th>Роль</th>
                <th>Логин</th>
                <th>Редактировать</th>
              </tr>
            </thead>
            <tbody>
              {usersList.length > 0 &&
                usersList.map((el, idx) => {
                  return (
                    <tr key={el._id}>
                      <td>{el.fio}</td>
                      <td>{el.speciality}</td>
                      <td>{handleRole(el.role)}</td>
                      <td>{el.email}</td>
                      <td className="edit-block">
                        <Button
                          type="submit"
                          onClick={() => {
                            setSelectedUserForm(el._id);
                          }}
                          variant="primary"
                        >
                          Редактировать
                        </Button>
                        <Button
                          variant="danger"
                          onClick={() => onDelete(el._id)}
                          className="edit-delete"
                        >
                          Удалить
                        </Button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </Table>
        )}
        {isFetchingUsersList && (
          <div className="loader-container">
            <Loader
              type="Oval"
              color="#00BFFF"
              height={100}
              width={100}
              timeout={0}
            />
          </div>
        )}
      </Container>
      {isRequestWorking && (
        <div className="loader-bg">
          <Loader
            type="Oval"
            color="#00BFFF"
            height={100}
            width={100}
            timeout={0}
          />
        </div>
      )}
      {selectedUserForm.length > 0 && (
        <div className="modal__backdrop">
          <div className="modal-container">
            <FForm
              onSubmit={onSubmit}
              initialValues={
                selectedUserForm == "create"
                  ? {
                      role: ROLES_OPTIONS[0],
                    }
                  : {
                      ...foundUser,
                      role: {
                        label: handleRole(foundUser.role),
                        value: foundUser.role,
                      },
                    }
              }
              render={({
                handleSubmit,
                form,
                submitting,
                pristine,
                values,
              }) => (
                <>
                  <div className="modal-header">
                    <h4>
                      {selectedUserForm == "create"
                        ? "Добавление пользователя"
                        : "Редактирование пользователя"}
                    </h4>
                  </div>
                  <div className="modal-body">
                    <form onSubmit={handleSubmit}>
                      <Field
                        name="fio"
                        validate={composeValidators(required, minLength(2))}
                      >
                        {({ input, meta }) => (
                          <Form.Group controlId="fio">
                            <Form.Label>ФИО</Form.Label>
                            <Form.Control
                              isInvalid={meta.error && meta.touched}
                              {...input}
                              type="text"
                              placeholder="Андреев Владимир Игоревич"
                            />
                            <Form.Control.Feedback type="invalid">
                              {meta.error}
                            </Form.Control.Feedback>
                          </Form.Group>
                        )}
                      </Field>
                      <Field
                        name="email"
                        validate={composeValidators(required, mustBeEmail)}
                      >
                        {({ input, meta }) => (
                          <Form.Group controlId="email">
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                              isInvalid={meta.error && meta.touched}
                              {...input}
                              type="text"
                              placeholder="v.i.andreev@bk.ru"
                            />
                            <Form.Control.Feedback type="invalid">
                              {meta.error}
                            </Form.Control.Feedback>
                          </Form.Group>
                        )}
                      </Field>
                      <Field
                        name="password"
                        validate={
                          selectedUserForm == "create"
                            ? composeValidators(required, minLength(3))
                            : null
                        }
                      >
                        {({ input, meta }) => (
                          <Form.Group controlId="password">
                            <Form.Label>Пароль</Form.Label>
                            <Form.Control
                              isInvalid={meta.error && meta.touched}
                              {...input}
                              type="text"
                              placeholder={
                                selectedUserForm == "create"
                                  ? "Чем сложнее, тем лучше!"
                                  : "Оставьте пустым, если не хотите менять пароль"
                              }
                            />
                            <Form.Control.Feedback type="invalid">
                              {meta.error}
                            </Form.Control.Feedback>
                          </Form.Group>
                        )}
                      </Field>
                      <Field name="speciality">
                        {({ input, meta }) => (
                          <Form.Group controlId="speciality">
                            <Form.Label>Должность / учёная степень</Form.Label>
                            <Form.Control
                              isInvalid={meta.error && meta.touched}
                              {...input}
                              type="text"
                              placeholder=""
                            />
                            <Form.Control.Feedback type="invalid">
                              {meta.error}
                            </Form.Control.Feedback>
                          </Form.Group>
                        )}
                      </Field>
                      <Field
                        defaultValue={ROLES_OPTIONS[0]}
                        initialValue={ROLES_OPTIONS[0]}
                        validate={required}
                        name="role"
                      >
                        {({ input, meta }) => (
                          <Form.Group controlId="role">
                            <CustomSelector
                              {...input}
                              options={ROLES_OPTIONS}
                              label="Роль в системе"
                            />
                            <Form.Control.Feedback type="invalid">
                              {meta.error}
                            </Form.Control.Feedback>
                          </Form.Group>
                        )}
                      </Field>
                    </form>
                    {userAdminError && (
                      <Alert variant="danger">{userAdminError}</Alert>
                    )}
                  </div>
                  <div className="modal-footer">
                    <Button variant="secondary" onClick={handleCloseModal}>
                      Отмена
                    </Button>
                    <Button
                      variant="primary"
                      type="submit"
                      disabled={submitting || pristine}
                      onClick={handleSubmit}
                    >
                      Сохранить
                    </Button>
                  </div>
                </>
              )}
            />
          </div>
        </div>
      )}
      <ToastContainer />
    </div>
  );
};
