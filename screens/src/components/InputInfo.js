import React from 'react'
import {Button, Navbar, Nav, Form} from 'react-bootstrap';
import './InputInfo.css'
import Logo from '../imgs/logo.png';
// import Logo from '../imgs/logo.png';

// const InfoItem = ({label, text}) => {
// 	const [open, setOpen] = useState(false);
// 	return (
// 			<>
// 				<Button
// 						onClick={() => setOpen(!open)}
// 						aria-controls="btn-open"
// 						aria-expanded={open}
// 				>
// 					{label}
// 				</Button>
// 				<Collapse in={open}>
// 					<div id="btn-open">
// 						{text}
// 					</div>
// 				</Collapse>
// 			</>
// 	)
// }

// const blogText = "Privet!"
// const confText = "Kak dela?"
// const posobiaText = "Matan - kruto!"

// const InputInfo = props => {
//         return (
//           <>
//           <div className="input-info-block">
//             <div className="block-blogs">
// 	            <InfoItem label="Cтатьи в журнале" text={blogText}/>
//             </div>

//             <div className="block-conferentions">
//             <InfoItem label="Конференции" text={confText}/>
//             </div>

//             <div className="block-posobia">
// 	            <InfoItem label="Пособия" text={posobiaText}/>
//             </div>
//             </div>
//           </>
//         );
//     }

// Всплывающие блоки при нажатии (код сверху)

const InputInfo = props => {
    return (
        <div className="select-items-page">
        <Navbar bg="dark" variant="dark" className="justify-content-between navbar">
            <Navbar.Brand href="#">
            <img
                src={Logo}
                width="110"
                height="30"
                className="d-inline-block align-top"
                alt="React Bootstrap logo"
            />
            </Navbar.Brand>
            <Nav className="mr-sm-3">
            <Form.Text className="text-light">
                Кафедра Математики
            </Form.Text>
            </Nav>
        </Navbar>
        <div className="buttons-click">
            <Button variant="primary" type="submit">
                Статьи в журнале
            </Button>
            <Button variant="primary" type="submit">
                Конференции
            </Button>
            <Button variant="primary" type="submit">
                Пособия
            </Button>
        </div>
        </div>        

    )
}

export default InputInfo
