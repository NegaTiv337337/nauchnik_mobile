import React from 'react'
import { Navbar, Nav, Form, Button} from 'react-bootstrap';
import './Registration.css'
import Logo from '../imgs/logo.png';


const Registration = props => (
    <div className="main">
    <Navbar bg="dark" variant="dark" className="justify-content-between navbar">
    <Navbar.Brand href="#">
    <img
        src={Logo}
        width="110"
        height="30"
        className="d-inline-block align-top"
        alt="React Bootstrap logo"
      />
    </Navbar.Brand>
    <Nav className="mr-sm-2">
      <Nav.Link href="#home">Вход</Nav.Link>
      <Nav.Link href="#features">Регистрация</Nav.Link>
    </Nav>
  </Navbar>
  <div className="container-main">
      <div className="registration-form">
                <Form className="registration-form-full">
                <Form.Group controlId="Registration-names">
                <Form.Label>ФИО</Form.Label>
                <Form.Control placeholder="Жукова Галина Севастьяновна" />
            </Form.Group>
                <Form.Group controlId="select-people-position">
                    <Form.Label>Ваша должность</Form.Label>
                    <Form.Control as="select">
                    <option>Преподаватель</option>
                    <option>Администратор</option>
                    <option>Ученик</option>
                    </Form.Control>
                </Form.Group>
            <Form.Group controlId="registration-email">
                <Form.Label>Ваша электронная почта</Form.Label>
                <Form.Control type="email" placeholder="Введите  E-mail" />
                <Form.Text className="text-muted">
                Мы не делимся вашими данными ни с кем
                </Form.Text>
            </Form.Group>
            <Form.Group controlId="enter-registration-password">
                <Form.Label>Придумайте пароль</Form.Label>
                <Form.Control type="password" placeholder="Введите пароль" />
            </Form.Group>
            <Form.Group controlId="repeat-registration-password">
                <Form.Label>Повторите пароль пароль</Form.Label>
                <Form.Control type="password" placeholder="Повторите пароль" />
            </Form.Group>
            <Button variant="primary" type="submit" className="registration-button">
                Зарегестрироваться
            </Button>
            </Form>
      </div>
  </div>
    </div>

)

export default Registration