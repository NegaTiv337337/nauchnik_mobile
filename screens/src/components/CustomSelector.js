import React, { useEffect } from "react";
import Select from "react-select";
import { Form } from "react-bootstrap";

export const CustomSelector = ({
  label,
  required,
  input,
  value,
  defaultValue,
  ...rest
}) => {
  console.log(rest);
  // useEffect(() => {
  //   if (typeof value === "string" || value == "" || value == null)
  //     value = defaultValue;
  // }, [value]);
  return (
    <>
      <Form.Label>
        {label} {required && <span className="asterisk">*</span>}
      </Form.Label>
      <Select
        className="selector"
        classNamePrefix="select"
        value={value}
        defaultValue={defaultValue}
        {...input}
        {...rest}
      />
    </>
  );
};
