import React from "react";
import { Navbar, Nav, Button, Col, Row, Container } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { http } from "../api/http.js";
import { handleLogout } from "../redux/actions";
import Logo from "../imgs/logo.png";

const handleFIO = (str) => {
  let arr = str.split(" ");
  if (arr.length == 3) {
    return `${arr[0]} ${arr[1][0]}. ${arr[2][0]}.`;
  } else {
    return str;
  }
};

export const Header = () => {
  let role = "guest";
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  if (user) {
    role = user.role;
  }
  const downloadReport = (l) => {
    const fileName = "otchet-pokazateli-kafedra.docx";
    http
      .get(l, {
        headers: {
          "Content-Disposition": `attachment; filename=${fileName}`,
          "Content-Type":
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
          Authorization: "Bearer " + localStorage.getItem("NAUCHNIK-JWT"),
        },
        responseType: "arraybuffer",
      })
      .then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();
      })
      .catch((err) => {
        console.error(err);
      });
  };
  return (
    <Navbar bg="dark" variant="dark" className="justify-content-between navbar">
      <Container fluid>
        <Row>
          <Col sm={12} md={2}>
            <Navbar.Brand href="/">
              <img
                src={Logo}
                width="110"
                height="30"
                className="d-inline-block align-top"
                alt="Московский Политех"
              />
            </Navbar.Brand>
          </Col>
          <Col md={10} sm={12}>
            <Nav className="mr-lg-8">
              {user && (
                <>
                  {role == "admin" && (
                    <a
                      href={`${process.env.REACT_APP_API_URL}/admin/report`}
                      download
                    >
                      <Button variant="primary" size="sm" className="mr-4">
                        Сгенерировать отчёт
                      </Button>
                    </a>
                  )}
                  <Nav.Item className="user-fio">
                    <p>Вы вошли как {handleFIO(user.fio)}</p>
                  </Nav.Item>
                  <Nav.Link onClick={() => dispatch(handleLogout)}>
                    Выход
                  </Nav.Link>
                </>
              )}
            </Nav>
          </Col>
        </Row>
      </Container>
    </Navbar>
  );
};
