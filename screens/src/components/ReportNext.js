import React from 'react'
import {Form, Button} from 'react-bootstrap';
import './ReportNext.css'
// import Logo from '../imgs/logo.png';
// import TeacherImg from '../imgs/teacer.jpg'


const ReportNext = props => (
    <div className="main-report">
        <div className="report-container">
            <h2><span>Шаг 2:</span> Выберите этапы, которые нужно добавить в отчет</h2>
            <Button variant="dark" type="button" className="select-all-button">
                Выбрать всё
            </Button>
            <div className="next-step-container">
                <Form className="states-checkbox">
                <Form.Group>
                    <Form.Check type="checkbox" label="Статьи в жураналах" />
                    <div className="states-under-block">
                        <Form.Check type="checkbox" label="Ссыла на публикацию" />
                        <Form.Check type="checkbox" label="Соавторы с кафедры" />
                        <Form.Check type="checkbox" label="Дата Публикации" />
                        <Form.Check type="checkbox" label="Площадка" />
                        <Form.Check type="checkbox" label="Дополнительная информация" />
                    </div>
                </Form.Group>
                </Form>
                <Form className="conferentions-checkbox">
                <Form.Group>
                    <Form.Check type="checkbox" label="Конференции" />
                    <div className="states-under-block">
                        <Form.Check type="checkbox" label="Название доклада" />
                        <Form.Check type="checkbox" label="Соавторы с кафедры" />
                        <Form.Check type="checkbox" label="Дата публикации" />
                        <Form.Check type="checkbox" label="Тип конференции" />
                        <Form.Check type="checkbox" label="Дополнительная информация" />
                    </div>
                </Form.Group>
                </Form>
                <Form className="posobia-checkbox">
                <Form.Group>
                    <Form.Check type="checkbox" label="Пособия" />
                    <div className="states-under-block">
                        <Form.Check type="checkbox" label="ISBN" />
                        <Form.Check type="checkbox" label="Соавторы с кафедры" />
                        <Form.Check type="checkbox" label="Дата публикации" />
                        <Form.Check type="checkbox" label="Тип пособия" />
                        <Form.Check type="checkbox" label="Дополнительная информация" />
                    </div>
                </Form.Group>
                </Form>
            </div>
            <Button variant="dark" type="button" className="take-report">
                Сформировать отчет
            </Button>
        </div>
    </div>
)

export default ReportNext