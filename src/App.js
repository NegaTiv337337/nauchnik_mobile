import React, { Component } from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Header } from "./components/Header";
import { Done } from "./done.js";
import { ProtectedRoute } from "./components/ProtectedRoute";
import { DataEntry } from "./data-entry.js";
import { Login } from "./login.js";
import { AdminPanel } from "./admin-panel.js";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

class App extends Component {
  render() {
    let role = null;
    if (this.props.user && this.props.user.role) {
      role = this.props.user.role;
    }
    return (
      <Router>
        <Header />
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/done" component={Done} />
          {role == "admin" && (
            <ProtectedRoute exact path="/" component={AdminPanel} />
          )}
          {role != "admin" && (
            <ProtectedRoute exact path="/" component={DataEntry} />
          )}
        </Switch>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user
  };
}

export default connect(mapStateToProps)(App);
