import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { Form, Button, Alert } from "react-bootstrap";
import Loader from "react-loader-spinner";
import "./login.css";
import { connect, useDispatch } from "react-redux";
import { handleLogin, remindFromLS } from "./redux/actions";

export const Login = connect((state) => ({
  isLogining: state.isLogining,
  loginError: state.loginError,
}))(({ isLogining, loginError }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    let token = localStorage.getItem("NAUCHNIK-JWT");
    let user = localStorage.getItem("NAUCHNIK-USER");
    if (token && user) {
      dispatch(remindFromLS(token, JSON.parse(user)));
    }
  }, []);
  const { register, handleSubmit, watch, errors } = useForm();
  const onSubmit = (data) => {
    dispatch(handleLogin(data));
  };
  return (
    <div className="main">
      <div className="container-main">
        <div className="enter-form">
          {!isLogining && (
            <Form onSubmit={handleSubmit(onSubmit)} className="enter-form-full">
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Электронная почта</Form.Label>
                <Form.Control
                  ref={register}
                  name="email"
                  type="email"
                  placeholder="Введите  E-mail"
                />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Пароль</Form.Label>
                <Form.Control
                  ref={register}
                  name="password"
                  type="password"
                  placeholder="Введите пароль"
                />
              </Form.Group>
              <Button
                onClick={() => {
                  dispatch(handleLogin);
                }}
                variant="primary"
                type="submit"
                className="enter-button"
              >
                Вход
              </Button>
              {loginError && (
                <Alert className="mt-4" variant="danger">
                  {loginError}
                </Alert>
              )}
              <Form.Text className="text-muted mt-4">
                Если вы забыли пароль - обратитесь на std@mospolytech.ru.
              </Form.Text>
            </Form>
          )}
          {isLogining && (
            <Loader
              type="Oval"
              color="#00BFFF"
              height={100}
              width={100}
              timeout={0}
            />
          )}
        </div>
      </div>
    </div>
  );
});
