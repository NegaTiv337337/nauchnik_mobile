import React from "react";
import { Route } from "react-router-dom";
import { useSelector } from "react-redux";
import { Login } from "../login.js";

export const ProtectedRoute = ({ component, ...options }) => {
  const isAuthenticated = useSelector(state => state.user);
  const finalComponent = isAuthenticated ? component : Login;

  return <Route {...options} component={finalComponent} />;
};
