import React from "react";
import { Navbar, Nav, Form, Button } from "react-bootstrap";
import "./Enter.css";
import Logo from "../imgs/logo.png";

const Enter = props => (
  <div className="main">
    <div className="container-main">
      <div className="enter-form">
        <Form className="enter-form-full">
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Ваша электронная почта</Form.Label>
            <Form.Control type="email" placeholder="Введите  E-mail" />
            <Form.Text className="text-muted">
              Мы не делимся вашими данными ни с кем
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Пароль</Form.Label>
            <Form.Control type="password" placeholder="Введите пароль" />
          </Form.Group>
          <Button variant="primary" type="submit" className="enter-button">
            Вход
          </Button>
        </Form>
      </div>
    </div>
  </div>
);

export default Enter;
