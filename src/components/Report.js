import React from 'react'
import {Form, Button} from 'react-bootstrap';
import './Report.css'
import TeacherImg from '../imgs/teacer.jpg'


const Report = props => (
    <div className="main-report">
        <div className="report-container">
            <h2><span>Шаг 1:</span> Выберите сотрудников, по которым нужно составить отчет</h2>
            <Button variant="dark" type="button" className="select-all-button">
                Выбрать всех
            </Button>
            <div className="teacher-card">
                <Form>
                    <Form.Group controlId="formBasicCheckbox">
                        <Form.Check type="checkbox" />
                    </Form.Group>
                </Form>
                <div className="teacer-card-image">
                    <img src={TeacherImg} alt=""/>
                </div>
                <div className="teacher-name">
                    <h4>Жукова Галина Севастьяновна</h4>
                </div>
                <div className="teacher-status">
                    <h4>Заведущая кафедрой, профессор</h4>
                </div>
            </div>
            <div className="teacher-card">
                <Form>
                    <Form.Group controlId="formBasicCheckbox">
                        <Form.Check type="checkbox" />
                    </Form.Group>
                </Form>
                <div className="teacer-card-image">
                    <img src={TeacherImg} alt=""/>
                </div>
                <div className="teacher-name">
                    <h4>Жукова Галина Севастьяновна</h4>
                </div>
                <div className="teacher-status">
                    <h4>Заведущая кафедрой, профессор</h4>
                </div>
            </div>
            <div className="teacher-card">
                <Form>
                    <Form.Group controlId="formBasicCheckbox">
                        <Form.Check type="checkbox" />
                    </Form.Group>
                </Form>
                <div className="teacer-card-image">
                    <img src={TeacherImg} alt=""/>
                </div>
                <div className="teacher-name">
                    <h4>Жукова Галина Севастьяновна</h4>
                </div>
                <div className="teacher-status">
                    <h4>Заведущая кафедрой, профессор</h4>
                </div>
            </div>
            <Button variant="dark" type="button" className="next-step">
                Следующий шаг
            </Button>
        </div>
    </div>
)

export default Report