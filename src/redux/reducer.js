import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  FORM_REQUEST,
  FORM_SUCCESS,
  FORM_FAIL,
  USERS_GET_REQUEST,
  USERS_GET_SUCCESS,
  USERS_GET_FAIL,
  USER_ADD_REQUEST,
  USER_ADD_SUCCESS,
  USER_ADD_FAIL,
  USER_EDIT_REQUEST,
  USER_EDIT_SUCCESS,
  USER_EDIT_FAIL,
  USER_DELETE_REQUEST,
  USER_DELETE_SUCCESS,
  USER_DELETE_FAIL,
  USER_LOGOUT,
  CHANGE_FILTERS,
  CHANGE_FORM_USER,
  REMIND_FROM_LS
} from "./actions";

export const initialState = {
  user: null,
  isLogining: false,
  isDownloading: false,
  isSubmitting: false,
  isChangingUser: false,
  isFormSuccessful: false,
  isRequestWorking: false,
  usersList: [],
  email: "",
  filters: {},
  password: "",
  selectedUserForm: "",
  loginError: null,
  downloadError: null,
  dataFormError: null,
  getUsersListError: null,
  userAdminError: null,
  userChangeError: null
};

export function rootReducer(state = initialState, action) {
  switch (action.type) {
    case USER_LOGOUT:
      return { ...state, user: null };
    case LOGIN_REQUEST:
      return { ...state, isLogining: true, loginError: "" };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLogining: false,
        user: action.payload.user,
        loginError: ""
      };
    case REMIND_FROM_LS:
      return {
        ...state,
        isLogining: false,
        user: action.payload.user,
        loginError: ""
      };

    case LOGIN_FAIL:
      return {
        ...state,
        isLogining: false,
        loginError: action.payload.message
      };
    case FORM_REQUEST:
      return { ...state, isSubmitting: true };
    case FORM_SUCCESS:
      return {
        ...state,
        isFormSuccessful: true,
        dataFormError: null,
        isSubmitting: false
      };
    case FORM_FAIL:
      return {
        ...state,
        isFormSuccessful: false,
        dataFormError: action.payload.message,
        isSubmitting: false
      };
    case USERS_GET_REQUEST:
      return { ...state, isFetchingUsersList: true };
    case USERS_GET_SUCCESS:
      return {
        ...state,
        usersList: action.payload,
        getUsersListError: null,
        isFetchingUsersList: false
      };
    case USERS_GET_FAIL:
      return {
        ...state,
        getUsersListError: action.payload.message,
        isFetchingUsersList: false
      };
    case USER_DELETE_REQUEST:
      return { ...state, isRequestWorking: true };
    case USER_DELETE_SUCCESS:
      return {
        ...state,
        userAdminError: null,
        isRequestWorking: false
      };
    case USER_DELETE_FAIL:
      return {
        ...state,
        userAdminError: action.payload.message,
        isRequestWorking: false
      };
    case USER_ADD_REQUEST:
      return { ...state, isRequestWorking: true };
    case USER_ADD_SUCCESS:
      return {
        ...state,
        userAdminError: null,
        isRequestWorking: false
      };
    case USER_ADD_FAIL:
      return {
        ...state,
        userAdminError: action.payload.message,
        isRequestWorking: false
      };
    case USER_EDIT_REQUEST:
      return { ...state, isRequestWorking: true };
    case USER_EDIT_SUCCESS:
      return {
        ...state,
        userAdminError: null,
        isRequestWorking: false
      };
    case USER_EDIT_FAIL:
      return {
        ...state,
        userAdminError: action.payload.message,
        isRequestWorking: false
      };
    case CHANGE_FILTERS:
      return {
        ...state,
        filters: action.payload
      };
    case CHANGE_FORM_USER:
      return {
        ...state,
        selectedUserForm: action.payload
      };
    default:
      return state;
  }
}
