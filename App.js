import { SafeAreaView } from "react-native-safe-area-context";
import React, { useState } from "react";
import { Image, Dimensions, Text } from "react-native";
import { WebView } from "react-native-webview";

const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

export default function App() {
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const onLoad = () => {
    setIsLoading(false);
  };
  const onError = (err) => {
    console.error(err, "err");
    setIsError(true);
  };
  return (
    <SafeAreaView
      style={{
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        flex: 1,
      }}
    >
      {isLoading && (
        <Image
          style={{
            width: deviceWidth * 0.5,
            height: deviceWidth * 0.5,
            position: "absolute",
            zIndex: 2,
          }}
          source={require("./assets/favicon.png")}
        />
      )}
      {isError && <Text>Error.</Text>}
      {!isError && (
        <WebView
          onLoadEnd={onLoad}
          onError={onError}
          allowFileAccess
          originWhitelist={["*"]}
          source={{ uri: "https://veryweb.site/?cache_bust=45345345" }}
          style={{ height: deviceHeight, width: deviceWidth }}
        />
      )}
    </SafeAreaView>
  );
}
